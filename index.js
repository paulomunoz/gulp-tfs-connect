'use strict';
var gutil = require('gulp-util');
var fs = require('fs');
var execSync = require('child_process').execSync;
var exec = require('child_process').exec;
var request = require('sync-request');
var packageFile = JSON.parse(fs.readFileSync('package.json'));
var affectedFilesList = '';
var jiraList;
var jiraIssuesList;  
var credentials;
var argv = require('yargs').argv;

try {
	credentials = JSON.parse(fs.readFileSync('.credentials'));
	} catch(e){
	try {
	  credentials = JSON.parse(fs.readFileSync('C:/.credentials'));
	} catch(e){}
};

module.exports = function (options) {
	/*if (!options.foo) {
		throw new gutil.PluginError('gulp-tfs-connect', '`foo` required');
	}*/

	//(filetypes, branchNames, jiras, comment)
	var filetypes = argv.f;
  var branchNames = argv.b;
  var jiras = argv.j;
  var comment = argv.c;

	options = options || {};

	options.rootfolder = options.rootfolder || "C:/TFS";
	options.branches = options.branches || {};
	options.branches.qapl = options.branches.qapl || "/EFLanguage.EFCom/Branches/QAPL";
	options.branches.qa2 = options.branches.qa2 || "/EFLanguage.EFCom/Branches/QA2";
	options.branches.trunk = options.branches.trunk || "/EFLanguage.EFCom/Trunk";

    // 
    try {
      var configFile = JSON.parse(fs.readFileSync('C:/.tfsconfig'));

      if (configFile && configFile.hasOwnProperty('rootfolder') && configFile.rootfolder != '') {
        options.rootfolder = configFile.rootfolder;
      }
    } catch(e){};
    
    // Builds array with filetypes to be checked in
    var filetypes = buildArrayList(filetypes, options.files);

    // If the list is not valid, show the error messages and stop execution
    if (!validateFiletypeList(filetypes, options)) {
      return;
    }

    // Builds array with branches to check in into
    var branchNames = buildArrayList(branchNames, options.branches)

    // If the list is not valid, show the error messages and stop execution
    if (!validateBranchList(branchNames, options)) {
      return;
    }

    // Export jira number to a variable with wider scope'
    if (jiras && jiras != 'none') {
      jiraList = jiras.split("/");
    }

    var paths = '';
   
    // For each branch
    branchNames.forEach(function(branch) {

      // For each filetype to be checked in
      filetypes.forEach(function(filetype) {
        // Array objects, with src and dest for each file
        var fileList = options.files[filetype];

        // If fileList is not an array
        if (!Array.isArray(fileList)) {

          // We put it inside an array so we can run through the loop normally
          fileList = [fileList];
        }

        //grunt.log.writeln(fileList[0]['src']);

        fileList.forEach(function(item) {
          var physicalPath = options.rootfolder + options.branches[branch] + item.dest;
          //grunt.log.writeln('Removing read-only from ' + physicalPath);
          console.log('Removing read-only from ' + physicalPath);
          execSync('attrib -r ' + physicalPath);

          paths += ' $' + options.branches[branch] + item.dest;
        });
      });
    });

    // Undo any pending changes
    runCommand('start cmd /c tf undo ' + paths + ' /noprompt');

    // Gets the Latest from TFS
    runCommand('tf get ' + paths + ' /force');

    // Checkout all files from TFS
    runCommand('tf checkout ' + paths);

    // For each Branch
    branchNames.forEach(function(branch) {
      // For each filetype to be checked in
      filetypes.forEach(function(filetype) {
        // Array objects, with src and dest for each file
        var fileList = options.files[filetype];

        // If fileList is not an array
        if (!Array.isArray(fileList)) {

          // We put it inside an array so we can run through the loop normally
          fileList = [fileList];
        }

        // For each File that is going to be checked in
        fileList.forEach(function(item) {
          // We get the local Path
          var localPath = item.src;
          // The TFS Path
          var tfsPath = options.rootfolder + options.branches[branch] + item.dest;

          affectedFilesList += '\n$' + options.branches[branch] + item.dest;

          // Log Message on screen
          //grunt.log.writeln('Copying "' + localPath + '" to "' + tfsPath);
          console.log('Copying "' + localPath + '" to "' + tfsPath);
          // And we copy the content of the local file to the TFS location
          fs.writeFileSync(tfsPath, fs.readFileSync(localPath));
        });
      });
    });

    // Checks Jiras has been specified
    if (jiraList && credentials) {
      comment = comment ? comment + ' / ' : '';
      // We Call the JIRA API Query for all provided Keys
      var res = request('GET', 'http://' + credentials.jira.user + ':' + credentials.jira.password + '@jira.ef.com/rest/api/2/search?jql=issueKey+in+(' + jiraList.join() + ')');
      // We parse the result
      jiraIssuesList = JSON.parse(res.getBody());

      // We build the comment using IssueKey: IssueSummary
      // So first loop through each issue
      jiraIssuesList.issues.forEach(function(issue, index) {
        // If it's NOT the first item, them add a ' / ' to the string
        if (index > 0) {
          comment += ' / ';
        }

        // Build the IssueKey: IssueSummary comment
        comment += issue.key + ': ' + issue.fields.summary;
      });
    }

    comment = comment || '';

    // Now comes the CHECK IN process
    // Because of current limitations we only add a callback if the Branch is called QAPL, so we check
    // if that is the case
    if (branchNames.length == 1 && branchNames[0] == 'qapl') {
      runCommand('tf checkin' + paths + ' /noprompt /bypass /comment:"' + comment + '"', checkin_callBack);
    } else {
      runCommand('start cmd /c tf checkin' + paths + ' /comment:"' + comment + '"');
      console.log(getNextReleasesFormattedComment());
    }


	/*return through.obj(function (file, enc, cb) {
		if (file.isNull()) {
			cb(null, file);
			return;
		}

		if (file.isStream()) {
			cb(new gutil.PluginError('gulp-tfs-connect', 'Streaming not supported'));
			return;
		}

		try {
			//file.contents = new Buffer(someModule(file.contents.toString(), options));
			this.push(file);
		} catch (err) {
			this.emit('error', new gutil.PluginError('gulp-tfs-connect', err));
		}

		cb();
	});*/
};


/*
   * Callback for the Checkin operation
   * 
   * @param stdout Text returned by the execution
   */
  function checkin_callBack(stdout) {
      // Retrieves the Changeset number from the text returned
      var changeset = stdout.match(/#([0-9]+)/);
      changeset = changeset ? changeset[1] : '';

      // If we have all needed information
      if (jiraIssuesList && changeset && credentials && credentials.jira && credentials.jira.user && credentials.jira.password) {
        var jiraMessage = 'Changeset: ' +  changeset;
        jiraMessage += '\n\n' + 'Files Changed: ' +affectedFilesList;
        jiraMessage += '\n\n' + getNextReleasesFormattedComment();
        

        jiraIssuesList.issues.forEach(function(issue, index) {

          var res = request('POST', 'http://'+credentials.jira.user+':'+credentials.jira.password+'@jira.ef.com/rest/api/2/issue/' + issue.key + '/comment', {
              body: JSON.stringify({
                  'body': jiraMessage
              }),
              headers: {
                  "Content-type": "application/json"
              }
          });

          //grunt.log.writeln(res.getBody());
        });
      }
  }

  /*
   * Builds a String with next Releases Dates.
   * 
   * @return string Text with information of next QA2, QA and Live Releases
   */
  function getNextReleasesFormattedComment() {
    var next = {
      sun: getNextDayOfWeek(0),
      mon: getNextDayOfWeek(1),
      tue: getNextDayOfWeek(2),
      wed: getNextDayOfWeek(3),
      thu: getNextDayOfWeek(4),
      fri: getNextDayOfWeek(5),
      sat: getNextDayOfWeek(6)
    };

    var qa2Date = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    var qaDate = new Date(Math.min(next.mon, next.wed));
    var liveDate = new Date((qa2Date.getDay() == 4) ? (next.thu.setDate(next.thu.getDate() + 7)) : next.thu );

    var text = 'QA2 Release Date: ' + formatDate(qa2Date);
    text += '\nQA Release Date: ' + formatDate(qaDate);
    text += '\nLive Release Date: ' + formatDate(liveDate);

    return text;
  }

  
  function runCommand(command, fnc) {
      // grunt.log.writeln('Running ' + command + '\n');
      console.log('Running ' + command + '\n');

      var stdout = execSync(command);
      //grunt.log.writeln(stdout);
      gutil.log(stdout.toString('utf8'));
      //process.stdout.write(stdout);

      if (fnc) fnc(stdout);

      //grunt.log.writeln('>> ------------\n'['green']);
      console.log('>> ------------\n');
  }


  /*
   * Build an array by spliting the string on the '/' character, if the string is not defined, or its value 
   * is 'all', loops through the properties of the provided object and add all of them
   * 
   * @param string String with the items separated by '/'
   * @param objWithAllProps Object to loop through and get all properties
   *
   * @return Array
   */
  function buildArrayList(string, objWithAllProps) {
    var finalList = [];

    if (!string || string == 'all') {
      finalList = [];
      for (var property in objWithAllProps) {
          if (objWithAllProps.hasOwnProperty(property)) {
              finalList.push(property);
          }
      }
    } else {
      finalList = string.split("/");
    }

    return finalList;
  }


  /*
   * Validates the filetype list requested
   * 
   * @param filetyes Array List of filetypes to be validated
   */
  function validateFiletypeList(filetypes, options) {
      var _return = true;

      // For each filetype requested
      filetypes.forEach(function(filetype) {
        var validExtensions = ['css', 'js', 'map', 'jpg', 'jpeg', 'png', 'eot', 'svg', 'ttf', 'woff', 'woff2', 'html'];

        // We first check if the filetype is not in the list of valid extensions
          if (validExtensions.indexOf(filetype) == -1) {
            // If so, show an error message and change return value to false
            //grunt.log.errorlns('Invalid File Type: ' + filetype);
            console.log('Invalid File Type: ' + filetype);

            _return = false;
          }

          // If the combination of "the filetype is set and it's not empty" (which is the expected) returns false,
          // then the filetype hasn't been set on the configuration file, or the string is empty
          if (!(options.files.hasOwnProperty(filetype) && options.files[filetype] != '')) {
            // If so, show an error message and change return value to false
            _return = false;
            //grunt.log.errorlns('File Type "' + filetype + '" not found in configuration file or the string is empty.');
            console.log('File Type "' + filetype + '" not found in configuration file or the string is empty.');
          }

      });

      return _return;
  }

  function validateBranchList(branchNames, options) {
      var _return = true;

      branchNames.forEach(function(branch) {
        // If the combination of "the branch is set and it's not empty" (which is the expected) returns false,
        // then the branch hasn't been set on the configuration file, or the string is empty
        if (!(options.branches.hasOwnProperty(branch) && options.branches[branch] != '')) {
          // If so, show an error message and change return value to false
          //grunt.log.errorlns('Branch "' + branch +'" not found in configuration file or the string is empty.');
          console.log('Branch "' + branch +'" not found in configuration file or the string is empty.');
          _return = false;
        }
      });

      return _return;
  }

  function getNextDayOfWeek(dayOfWeek) {
      var resultDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
      resultDate.setDate(resultDate.getDate() + (7 + dayOfWeek - resultDate.getDay()) % 7);
      return resultDate;
  }

  function formatDate(date) {
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
  }