# gulp-tfs-connect [![Build Status](https://travis-ci.org/<%= githubUsername %>/gulp-tfs-connect.svg?branch=master)](https://travis-ci.org/<%= githubUsername %>/gulp-tfs-connect)

> My <%= superb %> gulp plugin


## Install

```
$ npm install --save-dev gulp-tfs-connect
```


## Usage

```js
var gulp = require('gulp');
var tfsConnect = require('gulp-tfs-connect');

gulp.task('default', function () {
	return gulp.src('src/file.ext')
		.pipe(tfsConnect())
		.pipe(gulp.dest('dist'));
});
```


## API

### tfsConnect(options)

#### options

##### foo

Type: `boolean`  
Default: `false`

Lorem ipsum.


## License

MIT © [<%= name %>](<%= website %>)
